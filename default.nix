self: super:

rec {
  inkscapeExtensions = super.recurseIntoAttrs (super.callPackage ./pkgs/inkscape-extensions { });

  inkscapeWrapWithExtensions = import ./pkgs/build-support/inkscape-wrapper.nix {
    inherit (super) lib stdenv;
    inherit (super.xorg) lndir;
  };

  inkscapeWithExtensions = inkscapeWrapWithExtensions super.inkscape inkscapeExtensions;

}

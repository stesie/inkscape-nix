{ stdenv, fetchurl, fetchFromGitHub, unzip, pkgs }:

let
  extensionDerivation = args: stdenv.mkDerivation ({
    preferLocalBuild = true;
    installPhase = ''
      mkdir -p "$out/share/inkscape/extensions"
      cp -pvr *.inx *.py "$out/share/inkscape/extensions/"
      find "$out/share/inkscape/extensions/" -name "*.py" -exec chmod +x {} \;
    '';
  } // args);

  extensionPackages = {
    tabbed-box-maker = extensionDerivation rec {
      version = "0.94";
      name = "tabbed-box-maker-${version}";

      src = fetchFromGitHub {
        owner = "paulh-rnd";
        repo = "TabbedBoxMaker";
        rev = "fa933b443c9361c56e136b90ef7bac141cc1f88c";
        sha256 = "1jk43z2s4hmp50z1603a0knf539s6ckvh52pzg01m15rrnl61836";
      };
      meta = with stdenv.lib; {
        description = "Tool for creating boxes using tabbed construction";
        homepage = https://github.com/paulh-rnd/TabbedBoxMaker;
        license = with licenses; [ gpl2 ];
        platforms = platforms.all;
      };
    };

  };

in {
  inherit extensionDerivation;
 }
 // extensionPackages

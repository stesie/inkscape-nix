{ lib, lndir, stdenv }: inkscape: inkscapeExtensions:

extensionsFun: # packages explicitly requested by the user

let
  explicitRequires =
    if builtins.isFunction extensionsFun
    then extensionsFun inkscapeExtensions
    else extensionsFun;
in

stdenv.mkDerivation {
  inherit explicitRequires;

  # directly inherit name of inkscape package, we *must not* append a
  # "with-extensions" suffix or similar, since we'll be patching around
  # in the binaries (which would go wrong if store path lengths differs)
  inherit (inkscape) name meta;

  phases = "installPhase";

  nativeBuildInputs = [ lndir ];
  propagatedBuildInputs = [ inkscape ] ++ explicitRequires;

  installPhase = ''
    # Link Inkscape itself
    mkdir $out
    lndir -silent ${inkscape} $out

    # Bin-patch extensions directory in libinkscape_base.so
    rm -f $out/lib/inkscape/libinkscape_base.so
    sed -e "s#${inkscape}#$out#g" < ${inkscape}/lib/inkscape/libinkscape_base.so > $out/lib/inkscape/libinkscape_base.so
    rm -f $out/bin/inkscape
    sed -e "s#${inkscape}#$out#g" < ${inkscape}/bin/inkscape > $out/bin/inkscape
    chmod +x $out/bin/inkscape

    # Link requested packages.
    for pkg in $explicitRequires; do
      lndir -silent $pkg/share $out/share
    done
  '';
}

